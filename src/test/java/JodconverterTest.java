import static org.junit.Assert.assertEquals;

import org.junit.Test;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

public class JodconverterTest {

    @Test
    public void testHttpAvailable() throws IOException, InterruptedException {
        Thread.sleep(5*1000);
        String testUrl = "http://"+System.getProperty("jodconverter.host")+":"+System.getProperty("jodconverter.tcp.8080");
        URL obj = new URL(testUrl + "/converter");
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();
        int res = con.getResponseCode();
        assertEquals(200, res);
    }

}
