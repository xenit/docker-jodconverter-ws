FROM xeniteu/openjdk:jdk-7-centos-7

RUN yum install -y vim wget curl unzip libice6 libsm-dev fontconfig libxt6 libxfont1 cups-libs libxrender1 gsfonts cairo libSM && \
  wget -O /opt/libreoffice.tar.gz http://download.documentfoundation.org/libreoffice/stable/6.2.7/rpm/x86_64/LibreOffice_6.2.7_Linux_x86-64_rpm.tar.gz && \
  cd /opt && tar -xvf libreoffice.tar.gz && cd LibreOffice_6.2.7.1_Linux_x86-64_rpm && \
  yum localinstall -y RPMS/*.rpm 

RUN  wget -O /tmp/open-sans.zip https://www.fontsquirrel.com/fonts/download/open-sansopen-sans.zip \
  && mkdir /root/.fonts \
  && unzip /tmp/open-sans.zip -d /root/.fonts \
  && rm -fr open-sans.zip
RUN fc-cache -fv

RUN wget -O jod-tomcat.zip 'http://sourceforge.net/projects/jodconverter/files/JODConverter/2.2.2/jodconverter-tomcat-2.2.2.zip/download' \
  && unzip jod-tomcat.zip -d /usr/local/src \
  && ln -s /usr/local/src/jodconverter-tomcat-2.2.2/bin/startup.sh /usr/bin/jod \
  && rm jod-tomcat.zip

ADD start /usr/bin/start-jod
ADD applicationContext.xml /usr/local/src/jodconverter-tomcat-2.2.2/webapps/converter/WEB-INF/applicationContext.xml

EXPOSE 8080
HEALTHCHECK CMD curl -L --fail http://localhost:8080/converter/ || exit 1

ENTRYPOINT ["/bin/sh", "-c", "/usr/bin/start-jod"]
